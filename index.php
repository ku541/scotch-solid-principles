<?php

include_once('autoloader.php');

use App\Outputters\Sum;
use App\Calculators\SpatialQuantity;
use App\Shapes\TwoDimensional\Circle;
use App\Shapes\TwoDimensional\Square;
use App\Shapes\ThreeDimensional\Cuboid;

$spatialQuantity = new SpatialQuantity([
    new Circle(2),
    new Square(5),
    new Square(6),
    new Cuboid(1, 2, 3)
]);

$areaOutput = new Sum($spatialQuantity);

echo $areaOutput->json();