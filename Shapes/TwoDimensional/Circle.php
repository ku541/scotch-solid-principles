<?php

namespace App\Shapes\TwoDimensional;

use App\Contracts\Shape;
use App\Contracts\TwoDimensional;

class Circle implements TwoDimensional, Shape
{
    private $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function area()
    {
        return pi() * pow($this->radius, 2);
    }

    public function spatialQuantity()
    {
        return $this->area();
    }
}