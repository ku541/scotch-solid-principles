<?php

namespace App\Shapes\TwoDimensional;

use App\Contracts\Shape;
use App\Contracts\TwoDimensional;

class Square implements TwoDimensional
{
    public $length;

    public function __construct($length)
    {
        $this->length = $length;
    }

    public function area()
    {
        return pow($this->length, 2);
    }

    public function spatialQuantity()
    {
        return $this->area();
    }
}