<?php

namespace App\Shapes\ThreeDimensional;

use App\Contracts\Shape;
use App\Contracts\TwoDimensional;
use App\Contracts\ThreeDimensional;

class Cuboid implements TwoDimensional, ThreeDimensional, Shape
{
    public $width;
    public $height;
    public $length;

    public function __construct($width, $height, $length)
    {
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
    }

    public function area()
    {
        return 2 * (
            ($this->length * $this->width) +
            ($this->width * $this->height) +
            ($this->height * $this->length)
        );
    }

    public function volume()
    {
        return $this->width * $this->height * $this->length;
    }

    public function spatialQuantity()
    {
        return $this->area() + $this->volume();
    }
}