<?php

namespace App\Outputters;

use App\Calculators\SpatialQuantity;

class Sum
{
    private $areaSum;

    public function __construct(SpatialQuantity $areaSum)
    {
        $this->areaSum = $areaSum;
    }

    public function json()
    {
        return json_encode(['sum' => $this->areaSum->calculate()]);
    }
}