<?php

namespace App\Calculators;

use Exception;
use App\Contracts\TwoDimensional;
use App\Contracts\ThreeDimensional;

class SpatialQuantity
{
    private $shapes;

    public function __construct(Array $shapes)
    {
        $this->shapes = $shapes;
    }

    public function calculate()
    {
        if (! $this->shapes) {
            throw new Exception('Incalculable shapes encountered');
        }

        foreach ($this->shapes as $shape) {
            $area[] = $this->getSpatialQuantity($shape);
        }

        return array_sum($area);
    }

    public function getSpatialQuantity($shape)
    {
        if (! is_a($shape, TwoDimensional::class) && ! is_a($shape, ThreeDimensional::class)) {
            throw new Exception(
                'A shape must conform to a Shape & TwoDimensional or ThreeDimensional contract'
            );
        }

        return $shape->spatialQuantity();
    }
}