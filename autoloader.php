<?php

$classPaths = [
    '/Contracts/',
    '/Outputters/',
    '/Calculators/',
    '/Shapes/TwoDimensional/',
    '/Shapes/ThreeDimensional/',
];

define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);

spl_autoload_register('includeClassFiles');

function includeClassFiles($namespace) {
    global $classPaths;

    $className = getClassName($namespace);

    foreach ($classPaths as $classPath) {
        includeClassFile($classPath, $className);
    }
}

function getClassName($namespace) {
    $classPathParts = explode('\\', $namespace);

    if (! $classPathParts) {
        throw new \Exception('Namespace Error');
    }

    return end($classPathParts);
}

function includeClassFile($classPath, $className) {
    $absoluteClassPath = DOCUMENT_ROOT . $classPath . $className . '.php';

    if (file_exists($absoluteClassPath)) {
        include_once $absoluteClassPath;
    }
}