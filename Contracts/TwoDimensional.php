<?php

namespace App\Contracts;

interface TwoDimensional
{
    public function area();
}