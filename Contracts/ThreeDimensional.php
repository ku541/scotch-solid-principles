<?php

namespace App\Contracts;

interface ThreeDimensional
{
    public function volume();
}